/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

#ifndef YY_YY_HOME_NEO_WORK_AUTOSAR_AS_COM_AS_TOOL_KCONFIG_FRONTENDS_LIBS_PARSER_YCONF_H_INCLUDED
# define YY_YY_HOME_NEO_WORK_AUTOSAR_AS_COM_AS_TOOL_KCONFIG_FRONTENDS_LIBS_PARSER_YCONF_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    T_MAINMENU = 258,
    T_MENU = 259,
    T_ENDMENU = 260,
    T_SOURCE = 261,
    T_CHOICE = 262,
    T_ENDCHOICE = 263,
    T_COMMENT = 264,
    T_CONFIG = 265,
    T_MENUCONFIG = 266,
    T_HELP = 267,
    T_HELPTEXT = 268,
    T_IF = 269,
    T_ENDIF = 270,
    T_DEPENDS = 271,
    T_OPTIONAL = 272,
    T_PROMPT = 273,
    T_TYPE = 274,
    T_DEFAULT = 275,
    T_SELECT = 276,
    T_IMPLY = 277,
    T_RANGE = 278,
    T_VISIBLE = 279,
    T_OPTION = 280,
    T_ON = 281,
    T_WORD = 282,
    T_WORD_QUOTE = 283,
    T_UNEQUAL = 284,
    T_LESS = 285,
    T_LESS_EQUAL = 286,
    T_GREATER = 287,
    T_GREATER_EQUAL = 288,
    T_CLOSE_PAREN = 289,
    T_OPEN_PAREN = 290,
    T_EOL = 291,
    T_OR = 292,
    T_AND = 293,
    T_EQUAL = 294,
    T_NOT = 295
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 47 "/home/neo/work/autosar/as/com/as.tool/kconfig-frontends/libs/parser/yconf.y"

	char *string;
	struct file *file;
	struct symbol *symbol;
	struct expr *expr;
	struct menu *menu;
	const struct kconf_id *id;

#line 107 "/home/neo/work/autosar/as/com/as.tool/kconfig-frontends/libs/parser/yconf.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_HOME_NEO_WORK_AUTOSAR_AS_COM_AS_TOOL_KCONFIG_FRONTENDS_LIBS_PARSER_YCONF_H_INCLUDED  */
