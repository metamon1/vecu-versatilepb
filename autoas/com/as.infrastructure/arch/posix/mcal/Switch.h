#include <stdbool.h>

#define ON true
#define OFF false

bool AirbagState;
bool AutoCruiseState;
bool HighBeamState;
bool LowOilState;
bool PosLampState;
bool SeatbeltDriverState;
bool SeatbeltPassengerState;
bool TPMSState;
bool TurnLeftState;
bool TurnRightState;

static void Airbag_button() {
	if(AirbagState == OFF) {
		AirbagState = ON;
	} else if(AirbagState == ON) {
		AirbagState = OFF;
	}
}

static void AutoCruise_button() {
	if(AutoCruiseState == OFF) {
		AutoCruiseState = ON;
	} else if(AutoCruiseState == ON) {
		AutoCruiseState = OFF;
	}
}

static void HighBeam_button() {
	if(HighBeamState == OFF) {
		HighBeamState = ON;
	} else if(HighBeamState == ON) {
		HighBeamState = OFF;
	}
}

static void LowOil_button() {
	if(LowOilState == OFF) {
		LowOilState = ON;
	} else if(LowOilState == ON) {
		LowOilState = OFF;
	}
}

static void PosLamp_button() {
	if(PosLampState == OFF) {
		PosLampState = ON;
	} else if(PosLampState == ON) {
		PosLampState = OFF;
	}
}

static void SeatbeltDriver_button() {
	if(SeatbeltDriverState == OFF) {
		SeatbeltDriverState = ON;
	} else if(SeatbeltDriverState == ON) {
		SeatbeltDriverState = OFF;
	}
}

static void SeatbeltPassenger_button() {
	if(SeatbeltPassengerState == OFF) {
		SeatbeltPassengerState = ON;
	} else if(SeatbeltPassengerState == ON) {
		SeatbeltPassengerState = OFF;
	}
}

static void TPMS_button() {
	if(TPMSState == OFF) {
		TPMSState = ON;
	} else if(TPMSState == ON) {
		TPMSState = OFF;
	}
}

static void TurnLeft_button() {
	if(TurnLeftState == OFF) {
		TurnLeftState = ON;
		TurnRightState = OFF;

	} else if(TurnLeftState == ON) {
		TurnLeftState = OFF;
	}
}

static void TurnRight_button() {
	if(TurnRightState == OFF) {
		TurnRightState = ON;
		TurnLeftState = OFF;

	} else if(TurnRightState == ON) {
		TurnRightState = OFF;
	}
}