* * *
"""본 저장소는 과학기술통신부의 재원으로 정보통신기획평가원(IITP)의 지원을 받아 (주) 드림에이스, 경북대학교, 대구경북과학기술원이 함께 수행한 "차량 ECU 응용소프트웨어 개발 및 검증자동화를 위한 가상 ECU기반 차량레벨 통합 시뮬레이션 기술개발(과제고유번호: 2710008421)" 과제의 일환으로 공개된 오픈소스 코드입니다. (2022.04~2024.12)"""

"""본 저장소는 x86을 기반으로 한 Classic AUTOSAR 및 vECU를 위한 것으로 x86 환경에서 가상  ECU를 실행해보고 동작 수행을 확인하기 위한 용도로 활용합니다."""
* * *


# 빌드 환경 구축, 빌드, 실행
  
  
## 실행환경 
in Windows,
Env : VMWARE or WSL2  
OS : Ubuntu-22.04  
  
## 의존성 패키지

``` 
$ sudo apt install python2  
$ sudo apt install git  
$ sudo apt install scons  
$ sudo apt install pkg-config  
$ sudo apt-get install libgtk-3-dev  
$ sudo apt install gcc  
$ sudo apt-get install autoconf  
$ sudo apt-get install libtool  
$ sudo apt install make  
$ sudo apt install libc++-14-dev libc++abi-14-dev  
$ sudo apt install g++  

$ sudo apt-get install gtk+-3.0
$ sudo apt install nasm
$ sudo apt install vim
$ sudo apt install dbus-x11
```
  
## 보드 세팅 및 실행
  
third_party 참조를 위한 path 설정  
```
$ export PYTHONPATH=/home/${USER}/autosar/autoas/com/as.tool/config.infrastructure.system/third_party:${PYTHONPATH} 
```
  
보드세팅 : 현재는 각 레포지토리를 통해 다운 받은 경우 보드 세팅 및 릴리즈 방식 세팅이 되어 있는 상태임  
```
$ export BOARD=boardtype  
$ export RELEASE=releasetype  
```
  
실행  
```
scons  # 빌드  
scons run # 실행  
```

### POSIX

CAN 통신을 위한 소켓 실행 
- Scons run 하기 전에 실행
```
$ sudo modprobe can               # For WSL2   
$ sudo modprobe can_raw           # WSL2  
  
$ sudo modprobe vcan              # 실패할 경우 dmesg 확인 필요  
  
$ sudo ip link add dev can0 type vcan  
$ sudo ip link set up can0  
$ sudo ip link set can0 mtu 72  
$ sudo socket_lin_driver.exe 0  # /build/posix/socket_lin_driver.exe  
```
 $ sudo modprobe vcan
 $ sudo ip link add dev can0 type vcan
 $ sudo ip link set up can0
 $ sudo ip link set can0 mtu 72


### QEMU 구동을 위한 의존성

x86 및 versatilepb를 지원하기 위한 패키지 설치
```
$ sudo apt install gcc-arm-none-eabi
$ sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386  # 설치 실패 시 하단 참고 확인
```

[참고](https://stackoverflow.com/questions/58681718/intstalling-android-studio-on-64-bit-version-of-lubuntu-fails-installing-some-32)

### 기타 유용한 팀
  
##### CAN 통신 모니터링
```
# can-utils 설치하여 vcan test  
$ sudo apt install can-utils  

# can0로 아무 신호 생성하여 전송  
cangen can0  

# can0에서 vcan을 통해 나오는 신호 확인  
candump can0

# can0 인터페이스 확인 방법  
ifconfig  # 또는  
ip addr  

# can0 삭제  
$ sudo ip link del dev can0  
```
  
##### Scons 기타 명령어
```
scons -j$(expr $(nproc) - 1)  # 빠른 빌드를 위한 명령어  
```
  
### CAN 통신 설정

가상 CAN 인터페이스 생성   
```
sudo modprobe can
sudo modprobe can_raw

sudo modprobe vcan    # 실패할 경우 dmesg 확인 필요

sudo ip link add dev can0 type vcan
sudo ip link set up can0

sudo ip link set can0 mtu 72
```

vCAN통해 데이터 입력, 출력
```
# can-utils 설치하여 vcan test
sudo apt install can-utils

# can0로 데이터 전송
cansend can0 canid#message

# can0로 아무 신호 생성하여 전송
cangen can0

# can0에서 생성되는 신호 확인
candump [option] can0
## candump can0
```
vCAN 시뮬레이션
```
## candump -l can0    # can0 통해 오가는 데이터 기록
### L log2asc -I filename.log can0 # 로그데이터 포맷변경

# filename.log를 토대로 데이터 송수신 재현
canplayer -I filename.log

# 다른 CAN 인터페이스를 통해 송수신된 데이터를 활용한 재현
# can1의 로그 활용 can0에서 재현
canplayer can0=can1 -v -I candump-2015-03-20_123001.log


# can0 통해 송/수신된 제일 마지막 확인인 메세지
cansniffer can0
```
테스트 끝난 후 can0 삭제  

```
# can0 인터페이스 확인 방법
ifconfig
# 또는
ip addr

#can0 삭제
sudo ip link del dev can0
```

